package com.devcamp.api.countryregionapi.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.api.countryregionapi.models.Region;
import com.devcamp.api.countryregionapi.services.RegionService;

@CrossOrigin
@RestController
public class RegionController {
    @Autowired
    RegionService regionService;

    @GetMapping("/region-info")
    public Region getRegionByRegionCode(@RequestParam String regionCode) {        
        return regionService.getRegionByRegionCode(regionCode);
    }
}
